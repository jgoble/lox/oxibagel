# Chapter 22: Local Variables

- [Link to book chapter](https://craftinginterpreters.com/local-variables.html)
- [Previous chapter](21-global-variables.md)
- [Table of Contents](index.md)
- [Next chapter](23-jumping-back-and-forth.md)

## 22.1: Representing Local Variables

- I decided to make the compiler a member of the Parser struct. Time will tell if that is a good idea or not.

## 22.2: Block Statements

- Pretty simple.
- Rust does not have increment or decrement operators, so `+= 1` and `-= 1` are used instead.

## 22.3: Declaring Local Variables

- Straightforward implementation.
- We don't need the `identifiersEqual` function at all because Rust can compare strings for equality directly with `==`.

## 22.4: Using Locals

- Using a `Vec` for locals in the compiler means that we can use its iterators instead of manual indexing.
