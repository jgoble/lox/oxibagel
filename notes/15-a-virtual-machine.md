# Chapter 15: A Virtual Machine

- [Link to book chapter](https://craftinginterpreters.com/a-virtual-machine.html)
- [Previous chapter](14-chunks-of-bytecode.md)
- [Table of Contents](index.md)
- [Next chapter](16-scanning-on-demand.md)

## 15.1: An Instruction Execution Machine

- The Rust compiler contains a *borrow checker* that examines every reference in your program to ensure that the reference is dropped (freed) before the value itself is dropped by its owner. This works well for code within a function, but when a struct stores a reference, the borrow checker needs some help via *lifetime parameters* that tell it how the lifetime of the struct relates to the lifetime of the references within it.
- Lifetime parameters begin with a single apostrophe and are usually short, often a single letter like generic types. (Personally, I think they look ugly.)
- The book uses a static global variable for the VM. In Rust, this is a problem because the variable would have to be mutable, and reading or writing a mutable static variable requires an `unsafe` block. We don't want to have to scatter `unsafe` blocks all over our code, so we will not have a static variable. Instead, we'll declare it to be owned by `main()` and then pass references around.
- Rust macros are much more complex than C macros in that they are hygienic. Rather than try to attack them now, I'm choosing to convert some VM macros into functions, and trust that the Rust compiler will be smart enough to inline them where appropriate.
- Rust does conditional compilation using the `#[cfg(...)]` annotation and feature flags defined in Cargo.toml. These annotations can be applied to any block, which includes functions, methods, modules, etc.

## 15.2: A Value Stack Manipulator

- `Vec` saves us a bunch of trouble again by allowing us to eliminate the `STACK_MAX` and the need to manually track the stack top.
- `Vec::pop` returns an `Option` enum. This takes one of two forms: `Some(T)` for an element of type `T`, or `None` if nothing was found (i.e. in this case, the vector was empty). `Some(T)` must be unwrapped to produce the `T`; the best way to do this is by explicit matching, but here we know that our code will never pop without something on the stack, so we instead just `unwrap` the `Option` to get at the value implicitly. Be careful though, because if you unwrap a `None`, you get a panic (i.e. uncatchable exception).

## 15.3: An Arithmetic Calculator

- Back to enums: Rust traditionally uses PascalCase for Enum variants, rather than ALL_CAPS like in C. Annoyingly, Rust also automatically lints for this and sometimes throws warnings about it. So I switched the `Op` enum to use PascalCase for the opcodes instead of ALL_CAPS.
- Also, it's going to get painful fast to keep the `Op` enum and the match statement in `as_op` in sync. While Rust itself has no way to cast an int to an enum, there are third-party packages, called *crates*, that do this. I pulled in the `num_enum` crate, which enables me to annotate `Op` in a way that automatically derives the necessary match statement.
- Oddly enough, Rust's hygienic macros do in fact support passing arbitrary tokens to them, which enables our `binary_op!` macro. I didn't expect them to support that.
