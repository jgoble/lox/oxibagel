# Chapter 18: Types of Values

- [Link to book chapter](https://craftinginterpreters.com/types-of-values.html)
- [Previous chapter](17-compiling-expressions.md)
- [Table of Contents](index.md)
- [Next chapter](19-strings.md)

## 18.1: Tagged Unions

- Rust has traditional C-style unions, but they are unsafe: there is no way to know which field is the "active" field at any time, and therefore reading the wrong field can result in memory safety problems. Hence, every access to a union field in Rust requires an `unsafe` block, and they are intended mostly for Rust code that interoperates with C code. We want to avoid `unsafe` code here.
- Most C unions, like the ones used by clox and our Rust implementation, are *tagged unions*: a discriminant (often an enum value) and the union itself, with the discriminant telling the type actually stored in the union.
- Rust does have safe tagged unions: they are called `enum`s! Since `enum` variants can take a payload, they are essentially tagged unions: the variant name is the discriminant, and the payload of each is a field in the union. Because of the way enum variants are `match`ed, we can never access the "wrong" field, and therefore Rust enums are memory-safe tagged unions. Indeed, a little Googling shows that Rust enums are stored in the same form that a C tagged union would be: with a discriminant integer followed by the payload.

## 18.2: Lox Values and C Values

- All of these C macros that I secretly hated are no longer necessary:
  - `{BOOL,NIL,NUMBER}_VAL` are replaced by simply constructing the enum
  - `IS_{BOOL,NIL,NUMBER}` and `AS_{BOOL_NUMBER}` are replaced by pattern matching

## 18.3: Dynamically Typed Numbers

- The book's `runtimeError` function relies on varargs. Rust does not have vararg functions because they are inherently memory-unsafe. In order to do varargs in Rust, you have to use a macro.

## 18.4: Two New Types

- Enum variants can even have `impl`ementations that give them methods, such as `is_falsey` here.
- The whole `valuesEqual` function is eliminated by deriving the `PartialEq` trait on `Value`, which automatically compares the discriminants and then (if the discriminants are equal) the payloads, just like `valuesEqual` does.
- I chose to use separate opcodes for each comparison and equality operator instead of dealing with some operations emitting two opcodes and some emitting one. It's ultimately faster anyway and more amenable to future operator overloading.
