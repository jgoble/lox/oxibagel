# Chapter 20: Hash Tables

- [Link to book chapter](https://craftinginterpreters.com/strings.html)
- [Previous chapter](19-strings.md)
- [Table of Contents](index.md)
- [Next chapter](21-global-variables.md)

## General notes

- In Rust, we can use the `HashMap` from the standard library (`std::collections`) instead of building our own.
- The downside is that there is no easy way to obtain the hash of a string, making it pointless to intern strings for faster equality checks because it is O(n) no matter what (and Rust hides the details of string comparisons from us anyway).
- Hence, there's essentially nothing for us to do for this chapter. We *could* ignore `HashMap` and build our own hash table following the book's recipe so we could obtain the hash and take advantage of interning strings, but this project is all about taking advantage of what Rust can provide for us, so doing so would be against the spirit of this project.
