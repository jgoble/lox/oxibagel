use std::cell::RefCell;
use std::mem;
use std::str;

use crate::chunk;
use crate::chunk::Op;
use crate::scanner;
use crate::scanner::Token;
use crate::value;
use crate::value::Value;

pub fn compile(source: Vec<u8>) -> Option<value::Function> {
    let mut parser = Parser::new(source);
    parser.advance();
    while !parser.consume_if(Token::EndOfFile) && !*parser.had_error.borrow() {
        parser.declaration();
    }
    let function = parser.end_compiler();
    if *parser.had_error.borrow() {
        None
    } else {
        Some(function)
    }
}

#[derive(PartialEq, Copy, Clone)]
enum FuncType {
    Function,
    Script,
}

impl Default for FuncType {
    fn default() -> Self {
        FuncType::Function
    }
}

#[derive(Default)]
struct Compiler {
    enclosing: Option<Box<Compiler>>,
    function: value::Function,
    kind: FuncType,
    locals: Vec<Local>,
    depth: i32,
}

impl Compiler {
    fn new(kind: FuncType) -> Self {
        Compiler {
            enclosing: None,
            function: value::Function::new(),
            kind,
            locals: vec![Local {
                name: "".to_string(),
                depth: 0,
                is_captured: RefCell::new(false),
            }],
            depth: 0,
        }
    }
}

struct Local {
    name: String,
    depth: i32,
    is_captured: RefCell<bool>,
}

struct Parser {
    current: scanner::TokenResult,
    previous: scanner::TokenResult,
    had_error: RefCell<bool>,
    scanner: scanner::Scanner,
    compiler: Box<Compiler>,
}

impl Parser {
    fn new(source: Vec<u8>) -> Self {
        let scanner = scanner::Scanner::new(source);
        Parser {
            current: scanner.error_token("start of file"),
            previous: scanner.error_token("start of file"),
            had_error: RefCell::new(false),
            scanner,
            compiler: Box::new(Compiler::new(FuncType::Script)),
        }
    }

    fn current_chunk(&mut self) -> &mut chunk::Chunk {
        &mut self.compiler.function.chunk
    }

    fn advance(&mut self) {
        self.previous = self.current.clone();
        loop {
            self.current = self.scanner.scan_token();
            if let Err(ref error) = self.current {
                self.error_at_current(&error.message);
            } else {
                break;
            }
        }
    }

    fn check(&self, kind: Token) -> bool {
        match self.current {
            Ok(token) => token.kind == kind,
            Err(_) => false,
        }
    }

    fn consume_if(&mut self, kind: Token) -> bool {
        if self.check(kind) {
            self.advance();
            true
        } else {
            false
        }
    }

    fn consume(&mut self, kind: Token, message: &str) {
        match self.current {
            Ok(token) => if token.kind == kind {
                self.advance();
            } else {
                self.error_at_current(message);
            }
            Err(_) => {
                self.error_at_current(message);
            }
        }
    }

    fn error_at_current(&self, message: &str) {
        self.error_at(&self.current, message);
    }

    fn error(&self, message: &str) {
        self.error_at(&self.previous, message);
    }

    fn error_at(&self, token: &scanner::TokenResult, message: &str) {
        if *self.had_error.borrow() {
            return;
        }
        match token {
            Ok(tok) => {
                eprint!("[line {}] Error", tok.line);
                if tok.kind == Token::EndOfFile {
                    eprint!(" at end");
                } else {
                    eprint!(" at '{}'", self.lexeme(&token));
                }
            }
            Err(error) => {
                eprint!("[line {}] Error", error.line);
            }
        }
        eprintln!(": {}", message);
        self.had_error.replace(true);
    }

    fn emit_op(&mut self, op: Op) {
        let line = match &self.previous {
            Ok(token) => token.line,
            Err(error) => error.line,
        };
        self.current_chunk().write(op, line);
    }

    fn emit_constant(&mut self, value: Value) {
        let constant = self.make_constant(value);
        self.emit_op(Op::Constant(constant));
    }

    fn make_constant(&mut self, value: Value) -> u16 {
        let constant = self.current_chunk().add_constant(value);
        if constant > 0xFFFF {
            self.error("Too many constants in one chunk.");
            0
        } else {
            constant as u16
        }
    }

    fn emit_jump(&mut self, op: Op) -> usize {
        self.emit_op(op);
        self.current_chunk().code.len() - 1
    }

    fn patch_jump(&mut self, index: usize) {
        let jump = self.current_chunk().code.len() - index - 1;
        if jump > 0xFFFF {
            self.error("Too much code to jump over.");
            return;
        }
        let op = match self.current_chunk().code[index] {
            Op::Jump(0) => Op::Jump(jump as u16),
            Op::PopJumpIfFalse(0) => Op::PopJumpIfFalse(jump as u16),
            Op::JumpIfFalseOrPop(0) => Op::JumpIfFalseOrPop(jump as u16),
            Op::JumpIfTrueOrPop(0) => Op::JumpIfTrueOrPop(jump as u16),
            _ => unreachable!(),
        };
        self.current_chunk().code[index] = op;
    }

    fn emit_loop(&mut self, loop_start: usize) {
        let jump = self.current_chunk().code.len() - loop_start + 1;
        if jump > 0xFFFF {
            self.error("Loop body too large.");
        }
        self.emit_op(Op::JumpBack(jump as u16));
    }

    fn end_compiler(&mut self) -> value::Function {
        self.emit_return();
        let function = mem::take(&mut self.compiler.function);
        #[cfg(feature = "printcode")] {
            if !*self.had_error.borrow() {
                function.chunk.disassemble(if function.name == "" {
                    "<script>"
                } else {
                    &function.name
                });
            }
        }
        if let Some(enclosing) = mem::take(&mut self.compiler.enclosing) {
            self.compiler = enclosing;
        }
        function
    }

    fn emit_return(&mut self) {
        self.emit_op(Op::Nil());
        self.emit_op(Op::Return());
    }

    fn lexeme(&self, token: &scanner::TokenResult) -> String {
        if let Ok(token) = token {
            let lexeme = &self.scanner.source[token.start..token.end];
            let lexeme = str::from_utf8(lexeme);
            lexeme.unwrap().to_string()
        } else {
            unreachable!()
        }
    }

    // Variables

    fn parse_variable(&mut self, error: &str) -> u16 {
        self.consume(Token::Identifier, error);
        self.declare_variable();
        if self.compiler.depth > 0 {
            return 0;
        }
        self.identifier_constant(self.previous.clone())
    }

    fn identifier_constant(&mut self, name: scanner::TokenResult) -> u16 {
        self.make_constant(value::new_string(self.lexeme(&name)))
    }

    fn declare_variable(&mut self) {
        if self.compiler.depth == 0 {
            return;
        }
        let name = self.lexeme(&self.previous);
        let mut error = false;
        for local in self.compiler.locals.iter().rev() {
            if local.depth != -1 && local.depth < self.compiler.depth {
                break;
            }
            if local.name == name {
                error = true;
                break;
            }
        }
        if error {
            // Can't call this inside the loop because the iterator is an
            // immutable borrow
            self.error("Already a variable with this name in this scope.");
            return;
        }
        self.add_local(name);
    }

    fn define_variable(&mut self, global: u16) {
        if self.compiler.depth > 0 {
            self.mark_initialized();
            return;
        }
        self.emit_op(Op::DefineGlobal(global));
    }

    fn mark_initialized(&mut self) {
        if self.compiler.depth != 0 {
            self.compiler.locals.last_mut().unwrap().depth = self.compiler.depth;
        }
    }

    fn named_variable(&mut self, name: scanner::TokenResult) {
        let arg = self.resolve_local(&self.compiler, &self.lexeme(&name));
        if arg != -1 {
            self.emit_op(Op::GetLocal(arg as u16));
        } else {
            let arg = self.resolve_upvalue(&self.compiler, &self.lexeme(&name));
            if arg != -1 {
                self.emit_op(Op::GetUpvalue(arg as u16));
            } else {
                let arg = self.identifier_constant(name.clone());
                self.emit_op(Op::GetGlobal(arg));
            }
        }
    }

    fn begin_scope(&mut self) {
        self.compiler.depth += 1;
    }

    fn end_scope(&mut self) {
        self.compiler.depth -= 1;
        while let Some(local) = self.compiler.locals.last() {
            if local.depth <= self.compiler.depth {
                break;
            }
            if *local.is_captured.borrow() {
                self.emit_op(Op::CloseUpvalue());
            } else {
                self.emit_op(Op::Pop());
            }
            self.compiler.locals.pop();
        }
    }

    fn add_local(&mut self, name: String) {
        if self.compiler.locals.len() == 0xFFFF {
            self.error("Too many local variables in function.");
            return;
        }
        self.compiler.locals.push(Local {
            name,
            depth: -1,
            is_captured: RefCell::new(false),
        })
    }

    fn resolve_local(&self, compiler: &Box<Compiler>, name: &String) -> i32 {
        for (i, local) in compiler.locals.iter().enumerate().rev() {
            if *name == local.name {
                if local.depth == -1 {
                    self.error("Can't read local variable in its own initializer.");
                }
                return i as i32;
            }
        }
        return -1;
    }

    fn resolve_upvalue(&self, compiler: &Box<Compiler>, name: &String) -> i32 {
        let enclosing = match &compiler.enclosing {
            None => return -1,
            Some(c) => c,
        };
        let local = self.resolve_local(enclosing, name);
        if local != -1 {
            enclosing.locals[local as usize].is_captured.replace(true);
            return self.add_upvalue(compiler, local, true);
        }
        let upvalue = self.resolve_upvalue(enclosing, name);
        if upvalue != -1 {
            return self.add_upvalue(compiler, upvalue, false);
        }
        -1
    }

    fn add_upvalue(&self, compiler: &Box<Compiler>, index: i32, is_local: bool) -> i32 {
        for (i, upvalue) in compiler.function.upvalues.borrow().iter().enumerate() {
            if *upvalue == (index as u16, is_local) {
                return i as i32;
            }
        }
        let nup = compiler.function.upvalues.borrow().len();
        if nup > 0xFFFF {
            self.error("Too many closure variables in function.");
        }
        compiler.function.upvalues.borrow_mut().push((index as u16, is_local));
        nup as i32
    }

    // Statements

    fn declaration(&mut self) {
        if self.consume_if(Token::Fun) {
            self.fun_declaration();
        } else if self.consume_if(Token::Var) {
            self.var_declaration();
        } else {
            self.statement();
        }
    }

    fn fun_declaration(&mut self) {
        let global = self.parse_variable("Expect function name.");
        self.mark_initialized();
        self.function(FuncType::Function);
        self.define_variable(global);
    }

    fn function(&mut self, kind: FuncType) {
        let mut compiler = Box::new(Compiler::new(kind));
        compiler.enclosing = Some(mem::take(&mut self.compiler));
        if kind != FuncType::Script {
            compiler.function.name = self.lexeme(&self.previous);
        }
        self.compiler = compiler;
        self.begin_scope();

        self.consume(Token::LeftParen, "Expect '(' after function name.");
        if !self.check(Token::RightParen) {
            loop {
                self.compiler.function.arity += 1;
                if self.compiler.function.arity > 255 {
                    self.error_at_current("Can't have more than 255 parameters.");
                }
                let constant = self.parse_variable("Expect parameter name.");
                self.define_variable(constant);
                if !self.consume_if(Token::Comma) {
                    break;
                }
            }
        }
        self.consume(Token::RightParen, "Expect ')' after parameters.");
        self.consume(Token::LeftBrace, "Expect '{' before function body.");
        self.block();

        let function = self.end_compiler();
        let k = self.make_constant(value::new_function(function));
        self.emit_op(Op::Closure(k));
    }

    fn var_declaration(&mut self) {
        let global = self.parse_variable("Expect variable name.");
        if self.consume_if(Token::Equal) {
            self.expression();
        } else {
            self.emit_op(Op::Nil());
        }
        self.consume(Token::Semicolon, "Expect ';' after variable declaration.");
        self.define_variable(global);
    }

    fn statement(&mut self) {
        if self.consume_if(Token::Print) {
            self.print_statement();
        } else if self.consume_if(Token::For) {
            self.for_statement();
        } else if self.consume_if(Token::If) {
            self.if_statement();
        } else if self.consume_if(Token::Return) {
            self.return_statement();
        } else if self.consume_if(Token::While) {
            self.while_statement();
        } else if self.consume_if(Token::LeftBrace) {
            self.begin_scope();
            self.block();
            self.end_scope();
        } else {
            self.expression_statement();
        }
    }

    fn block(&mut self) {
        while !self.check(Token::RightBrace) && !self.check(Token::EndOfFile) {
            self.declaration();
        }
        self.consume(Token::RightBrace, "Expect '}' after block.");
    }

    fn print_statement(&mut self) {
        self.expression();
        self.consume(Token::Semicolon, "Expect ';' after value.");
        self.emit_op(Op::Print());
    }

    fn for_statement(&mut self) {
        self.begin_scope();
        self.consume(Token::LeftParen, "Expect '(' after 'for'.");
        if self.consume_if(Token::Semicolon) {
            // No initializer
        } else if self.consume_if(Token::Var) {
            self.var_declaration();
        } else {
            self.expression_statement();
        }

        let mut loop_start = self.current_chunk().code.len();
        let mut exit_jump: isize = -1;
        if !self.consume_if(Token::Semicolon) {
            self.expression();
            self.consume(Token::Semicolon, "Expect ';' after loop condition.");
            exit_jump = self.emit_jump(Op::PopJumpIfFalse(0)) as isize;
        }

        if !self.consume_if(Token::RightParen) {
            let body_jump = self.emit_jump(Op::Jump(0));
            let increment_start = self.current_chunk().code.len();
            self.expression();
            self.emit_op(Op::Pop());
            self.consume(Token::RightParen, "Expect ')' after for clauses.");

            self.emit_loop(loop_start);
            loop_start = increment_start;
            self.patch_jump(body_jump);
        }

        self.statement();
        self.emit_loop(loop_start);

        if exit_jump != -1 {
            self.patch_jump(exit_jump as usize);
        }

        self.end_scope();
    }

    fn if_statement(&mut self) {
        self.consume(Token::LeftParen, "Expect '(' after 'if'.");
        self.expression();
        self.consume(Token::RightParen, "Expect ')' after condition.");

        let then_jump = self.emit_jump(Op::PopJumpIfFalse(0));
        self.statement();

        let mut else_jump = 0;
        if self.check(Token::Else) {
            else_jump = self.emit_jump(Op::Jump(0));
        }

        self.patch_jump(then_jump);

        if self.consume_if(Token::Else) {
            self.statement();
            self.patch_jump(else_jump);
        }
    }

    fn return_statement(&mut self) {
        if self.compiler.kind == FuncType::Script {
            self.error("Can't return from top-level code.");
        }
        if self.consume_if(Token::Semicolon) {
            self.emit_return();
        } else {
            self.expression();
            self.consume(Token::Semicolon, "Expect ';' after return value.");
            self.emit_op(Op::Return());
        }
    }

    fn while_statement(&mut self) {
        let loop_start = self.current_chunk().code.len();
        self.consume(Token::LeftParen, "Expect '(' after 'while'.");
        self.expression();
        self.consume(Token::RightParen, "Expect ')' after condition.");

        let exit_jump = self.emit_jump(Op::PopJumpIfFalse(0));
        self.statement();
        self.emit_loop(loop_start);

        self.patch_jump(exit_jump);
    }

    fn expression_statement(&mut self) {
        self.expression();
        self.consume(Token::Semicolon, "Expect ';' after expression.");
        self.emit_op(Op::Pop());
    }

    // Expressions

    fn expression(&mut self) {
        self.assignment();
    }

    fn assignment(&mut self) {
        self.logic_or();
        if self.check(Token::Equal) {
            if let Ok(token) = self.previous {
                if token.kind == Token::RightParen {
                    // Can't assign to a grouping
                    self.error_at_current("Invalid assignment target.");
                    return;
                }
            }
            self.advance();
            let last = self.current_chunk().code.len();
            if last == 0 {
                self.error("Invalid assignment target.");
                return;
            }
            match self.current_chunk().code[last - 1] {
                Op::GetGlobal(k) => {
                    self.current_chunk().code.pop();
                    self.assignment();
                    self.emit_op(Op::SetGlobal(k));
                }
                Op::GetLocal(k) => {
                    self.current_chunk().code.pop();
                    self.assignment();
                    self.emit_op(Op::SetLocal(k));
                }
                Op::GetUpvalue(k) => {
                    self.current_chunk().code.pop();
                    self.assignment();
                    self.emit_op(Op::SetUpvalue(k));
                }
                _ => self.error("Invalid assignment target.")
            }
        }
    }

    fn logic_or(&mut self) {
        self.logic_and();
        if self.consume_if(Token::Or) {
            let end_jump = self.emit_jump(Op::JumpIfTrueOrPop(0));
            self.logic_or();
            self.patch_jump(end_jump);
        }
    }

    fn logic_and(&mut self) {
        self.equality();
        if self.consume_if(Token::And) {
            let end_jump = self.emit_jump(Op::JumpIfFalseOrPop(0));
            self.logic_and();
            self.patch_jump(end_jump);
        }
    }

    fn equality(&mut self) {
        self.comparison();
        loop {
            let opcode = match self.current {
                Ok(token) if token.kind == Token::EqualEqual => Op::Equal(),
                Ok(token) if token.kind == Token::BangEqual => Op::NotEqual(),
                _ => return,
            };
            self.advance();
            self.comparison();
            self.emit_op(opcode);
        }
    }

    fn comparison(&mut self) {
        self.term();
        loop {
            let opcode = match self.current {
                Ok(token) if token.kind == Token::Greater => Op::Greater(),
                Ok(token) if token.kind == Token::GreaterEqual => Op::GreaterEqual(),
                Ok(token) if token.kind == Token::Less => Op::Less(),
                Ok(token) if token.kind == Token::LessEqual => Op::LessEqual(),
                _ => return,
            };
            self.advance();
            self.term();
            self.emit_op(opcode);
        }
    }

    fn term(&mut self) {
        self.factor();
        loop {
            let opcode = match self.current {
                Ok(token) if token.kind == Token::Plus => Op::Add(),
                Ok(token) if token.kind == Token::Minus => Op::Subtract(),
                _ => return,
            };
            self.advance();
            self.factor();
            self.emit_op(opcode);
        }
    }

    fn factor(&mut self) {
        self.unary();
        loop {
            let opcode = match self.current {
                Ok(token) if token.kind == Token::Star => Op::Multiply(),
                Ok(token) if token.kind == Token::Slash => Op::Divide(),
                _ => return,
            };
            self.advance();
            self.unary();
            self.emit_op(opcode);
        }
    }

    fn unary(&mut self) {
        let opcode = match self.current {
            Ok(token) if token.kind == Token::Minus => Op::Negate(),
            Ok(token) if token.kind == Token::Bang => Op::Not(),
            _ => return self.call(),
        };
        self.advance();
        self.unary();
        self.emit_op(opcode);
    }

    fn call(&mut self) {
        self.primary();
        loop {
            if self.consume_if(Token::LeftParen) {
                let argc = self.argument_list();
                self.emit_op(Op::Call(argc));
            } else {
                break;
            }
        }
    }

    fn argument_list(&mut self) -> u8 {
        let mut argc = 0;
        if !self.check(Token::RightParen) {
            loop {
                argc += 1;
                if argc > 255 {
                    self.error_at_current("Can't have more than 255 arguments.");
                }
                self.expression();
                if !self.consume_if(Token::Comma) {
                    break;
                }
            }
        }
        self.consume(Token::RightParen, "Expect ')' after arguments.");
        argc as u8
    }

    fn primary(&mut self) {
        match self.current {
            Ok(token) if token.kind == Token::Nil => {
                self.advance();
                self.emit_op(Op::Nil());
            }
            Ok(token) if token.kind == Token::True => {
                self.advance();
                self.emit_op(Op::True());
            }
            Ok(token) if token.kind == Token::False => {
                self.advance();
                self.emit_op(Op::False());
            }
            Ok(token) if token.kind == Token::Number => {
                let number = self.lexeme(&self.current);
                let number = number.parse::<f64>().unwrap();
                self.advance();
                self.emit_constant(Value::Number(number));
            }
            Ok(token) if token.kind == Token::String => {
                // Convert manually since we have to strip the quote marks
                let string = &self.scanner.source[token.start + 1..token.end - 1];
                let string = str::from_utf8(string).unwrap().to_string();
                self.advance();
                self.emit_constant(value::new_string(string));
            }
            Ok(token) if token.kind == Token::Identifier => {
                self.advance();
                self.named_variable(self.previous.clone());
            }
            Ok(token) if token.kind == Token::LeftParen => {
                self.advance();
                self.expression();
                self.consume(Token::RightParen, "Expect ')' after expression.");
            }
            _ => {
                self.advance();
                self.error("Expect expression.");
            }
        }
    }
}
