#![allow(dead_code)]

// All top-level module declarations go in main.rs
// Those modules can then import each other by "use crate::modname"
// https://stackoverflow.com/a/46829631/
mod chunk;
mod compiler;
mod scanner;
mod value;
mod vm;

use std::io::prelude::*;

fn main() {
    let argv: Vec<String> = std::env::args().collect();
    let argc = argv.len();

    match argc {
        1 => repl(),
        2 => run_file(&argv[1]),
        _ => {
            eprintln!("Usage: bagel [path]");
            std::process::exit(64);
        }
    }

    // let mut vm = vm::VM::new(&chunk);
    // vm.interpret();
}

fn repl() {
    let mut vm = vm::VM::new();
    print!("> ");
    std::io::stdout().flush().unwrap();
    for line in std::io::stdin().lock().lines() {
        if let Err(msg) = line {
            eprintln!("{}", msg);
            std::process::exit(1);
        }
        let line = line.unwrap().as_bytes().to_vec();
        vm.interpret(line);
        print!("> ");
        std::io::stdout().flush().unwrap();
    }
    println!("");
}

fn run_file(path: &String) {
    let mut vm = vm::VM::new();
    let source = read_file(path);
    std::process::exit(match vm.interpret(source) {
        vm::InterpretResult::CompileError => 65,
        vm::InterpretResult::RuntimeError => 70,
        vm::InterpretResult::Ok => 0,
    })
}

// From example at:
// https://doc.rust-lang.org/rust-by-example/std_misc/file/open.html
fn read_file(path: &String) -> Vec<u8> {
    let path = std::path::Path::new(path);
    let mut file = match std::fs::File::open(path) {
        Err(msg) => {
            eprintln!("{}", msg);
            std::process::exit(74);
        }
        Ok(file) => file,
    };
    let mut source: Vec<u8> = Vec::new();
    if let Err(msg) = file.read_to_end(&mut source) {
        eprintln!("{}", msg);
        std::process::exit(74);
    }
    source
}
